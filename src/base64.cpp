// According includes
#include "base64.hpp"

// STL includes
#include <vector>

const std::string base64_chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

std::string base64_encode(const char *in, std::size_t len)
{
    std::string ret;
    std::size_t bytesLeft = len % 3;

    for(std::size_t i = 0; i < len; i += 3) {
        std::vector<char> plain(3);
        std::vector<char> encoded(4);
        std::size_t paddingSize = 0;

        plain.at(0) = in[i];

        if(i + 1 < len && i + 2 < len) {
            plain.at(1) = in[i + 1];
            plain.at(2) = in[i + 2];
        } else {
            if(bytesLeft == 1) {
                paddingSize = 2;
            } else if(bytesLeft == 2) {
                paddingSize = 1;
                plain.at(1) = in[i + 1];
            }
        }

        encoded.at(0) = ((plain.at(0) & 0xfc) >> 2);
        encoded.at(1) = ((plain.at(0) & 0x03) << 4) | ((plain.at(1) & 0xf0) >> 4);
        encoded.at(2) = ((plain.at(1) & 0x0f) << 2) | ((plain.at(2) & 0xc0) >> 6);
        encoded.at(3) = ((plain.at(2) & 0x3f));

        for(std::size_t j = 0; j < encoded.size() - paddingSize; j++) {
            ret += base64_chars.at(encoded.at(j));
        }
        for(std::size_t j = 0; j < paddingSize; j++) {
            ret += '=';
        }
    }

    return ret;
}

std::string base64_encode(const std::string &str)
{
    return base64_encode(str.c_str(), str.length());
}

std::string base64_decode(const char *in, std::size_t len)
{
    std::string ret;

    for(std::size_t i = 0; i < len; i += 4) {
        std::vector<char> encoded(4);
        std::vector<char> decoded(3);
        std::size_t paddingSize = 0;

        for(std::size_t j = 0; j < 4; j++) {
            if(i + j < len) {
                std::size_t position = base64_chars.find(in[i + j]);

                if(position != std::string::npos) {
                    encoded.at(j) = static_cast<char>(position);
                } else {
                    paddingSize++;
                }
            } else {
                paddingSize++;
            }
        }

        decoded.at(0) = ((encoded.at(0) & 0x3f) << 2) | ((encoded.at(1) & 0x30) >> 4);
        decoded.at(1) = ((encoded.at(1) & 0x0f) << 4) | ((encoded.at(2) & 0x3c) >> 2);
        decoded.at(2) = ((encoded.at(2) & 0x03) << 6) | ((encoded.at(3) & 0x3f));

        ret.insert(std::end(ret), std::begin(decoded), std::end(decoded) - paddingSize);
    }

    return ret;
}

std::string base64_decode(const std::string &str)
{
    return base64_decode(str.c_str(), str.length());
}

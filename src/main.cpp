// Own includes
#include <base64.hpp>

// STL includes
#include <iostream>

int main(int argc, char **argv) {
    std::string str = "123123123123123123123123123";
    std::size_t encodeCount = 5;

    std::cout << base64_encode("HALLO") << std::endl;
    std::cout << base64_encode("HELL") << std::endl;

    std::cout << base64_decode("SEFMTE8") << std::endl;
    std::cout << base64_decode("SEFMTE8=") << std::endl;

    std::cout << base64_decode("SEVMTA") << std::endl;
    std::cout << base64_decode("SEVMTA==") << std::endl;

//    if(argc > 1)
//        str = std::string(argv[1]);

//    if(argc > 2)
//        encodeCount = std::stoi(argv[2]);

//    std::cout << str << std::endl;

//    std::string encoded = str;
//    for(std::size_t i = 0; i < encodeCount; i++) {
//        encoded = base64_encode(encoded);
//        std::cout << encoded << std::endl;
//    }

//    std::string decoded = encoded;
//    for(std::size_t i = 0; i < encodeCount; i++) {
//        decoded = base64_decode(decoded);
//        std::cout << decoded << std::endl;
//    }

//    if(decoded == str) {
//        std::cout << "DECODED STRING IS SAME AS STRING" << std::endl;
//    } else if(base64_encode(decoded) == str) {
//        std::cout << "DECODED STRING ENCODED IS SAME AS STRING" << std::endl;
//    }

    return 0;
}

#ifndef BASE64_HPP
#define BASE64_HPP

// STL includes
#include <string>

std::string base64_encode(const char *in, std::size_t len);
std::string base64_encode(const std::string &str);
std::string base64_decode(const char *in, std::size_t len);
std::string base64_decode(const std::string &str);

#endif // BASE64_HPP
